/**
 * Created by Akme on 01.05.15.
 */


/**
 * User events processors
 */
$(document).ready(function(){

    /**
     * Reply function for status
     */
    $('.answer-btn').click(function(){
        var element = $(this);
        var id = element.data('id');
        var text = window.prompt('Please, enter an reply text');

        if( text ) {
            $.post('',{'id':id, 'act':'reply', 'text':text}, function( $response ){
                if( typeof $response.errors == 'undefined' ) {
                    element.addClass('btn-info');
                }
            }, "json");
        }
    });

    /**
     * Update statuses - favorite + retweet
     */
    $('.twitter-btn').click(function(){
        var element = $(this);
        var id = element.data('id');
        var number = element.data('count');
        var title = element.data('title');
        var action = element.data('action');
        var shiftDown = element.data('flow')=='neg';

        $.post('',{'id':id, 'act':action}, function( $response ){
            if( typeof $response.errors == 'undefined' ) {
                element.val(title+' [' + (shiftDown?--number:++number) + ']');
                element.data('count', number);
                if(shiftDown) {
                    element.removeClass('btn-success').addClass('btn-default');
                } else {
                    element.addClass('btn-success').removeClass('btn-default');
                }

                switch( action ) {
                    case 'favorite':
                    {
                        element.data('action','unfavorite');
                        element.data('flow','neg');
                        break;
                    }
                    case 'unfavorite':
                    {
                        element.data('action','favorite');
                        element.data('flow','pos');
                        break;
                    }

                }
            }
        }, "json");
    });

});