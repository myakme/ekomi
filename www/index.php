<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 30.04.15
 * Time: 18:11
 *
 * Initialization of the application
 */

header('Content-Type:text/html; charset=utf8');

$root = dirname(__FILE__);

include_once( $root .'/../autoload.php' );
$config = parse_ini_file( $root . '/../config.ini', true );

$database = PDOWrapper::setConnection( $config['mysql'] );
$storage = new \Model\Storage( $database );

$twitter = new \Model\Twitter( $config['twitterAPI'] );

App::initialize( $storage, $twitter, $config['application'] );
App::run();