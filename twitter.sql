-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 03 2015 г., 17:33
-- Версия сервера: 5.5.41-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `twitter`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tweets`
--

CREATE TABLE IF NOT EXISTS `tweets` (
  `id` bigint(20) unsigned NOT NULL,
  `text` varchar(160) NOT NULL,
  `retweet_count` int(11) NOT NULL,
  `favorite_count` int(11) NOT NULL,
  `favorited` tinyint(1) NOT NULL,
  `retweeted` tinyint(1) NOT NULL,
  `user` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tweets`
--

INSERT INTO `tweets` (`id`, `text`, `retweet_count`, `favorite_count`, `favorited`, `retweeted`, `user`) VALUES
(544867816504328192, 'Our #customerexperience and #customerservice is 5 * according to our #customers. Many new @eKomi #reviews http://t.co/i7XwVtpfm0', 3, 1, 0, 1, 231846311),
(544869198405836800, 'Sterne für Produkte und Services: #ERGO bittet Kunden um ihre Meinung http://t.co/7tqFcmjh3F #Blog @eKomi http://t.co/x30hmAS4dZ', 3, 0, 0, 1, 2292805620),
(544869572562939904, 'We have just had our 500th review on @eKomi - 100% positive with 4.9 out of 5 average score. Thank you all so much http://t.co/PyCNJFSz6i', 3, 2, 0, 1, 461401609),
(562591380040478720, 'eKomi The Feedback: eKomi "The Feedback Company" und Barketing etablieren eine.. http://t.co/ZHTEvdBTNR http://t.co/YPvT40lCeE', 3, 2, 0, 1, 44894572),
(562591820740177920, 'Les clients #Allianz donnent leur avis sur eKomi http://t.co/tiCE6ctPy6 http://t.co/pco1ZhIHY9', 5, 4, 0, 0, 2316895484),
(577394314768199680, 'Que pensent les clients #Allianz ? http://t.co/oTQM48FDlM Accrédité par eKomi http://t.co/umcMvZUz7E', 1, 2, 0, 0, 2316895484),
(577394453977120768, 'Les clients #Allianz donnent leur avis sur eKomi http://t.co/jJJP61D5c0 avec .@AllianzAvecVous http://t.co/0GMtVbiISS', 6, 5, 0, 0, 270453413),
(577757301181956097, '20 Must-Have Tools for Clever Marketers http://t.co/lxDXmfaN0v via @marketingprofs', 0, 0, 0, 0, 19068239),
(578142598797402113, 'Join eKomi @iwkongress in Munich, March 24th &amp; 25th in Hall A w/ our partner EHI! We look fwd to seeing you! #iwk http://t.co/slZOW3N4OF', 0, 0, 0, 0, 19068239),
(581430774479646720, 'eKomi''s glad to be a partner! ''''@Google Expands Product Rating Stars In Google Shopping Ads To UK, France, Germany http://t.co/rqz0cm7vXi''''', 0, 0, 0, 0, 19068239),
(581431103401103360, 'Product rating stars come to @Google shopping ads in the UK, France, &amp; Germany: http://t.co/FNNnSm0G79 http://t.co/0n4pTS311r', 33, 11, 0, 1, 1059801),
(581435837243658240, '#Google adds product ratings to search ads in #Europe: http://t.co/K0UGnHiZOS http://t.co/jPl8ILhjpx', 6, 2, 0, 1, 1407572472),
(582846222328823808, 'eKomi The Feedback: Kooperation mit Google: eKomi führt Produktbewertungen bei.. http://t.co/abXSavl7zu http://t.co/brO0850jYo', 2, 1, 0, 1, 44894572),
(582856142809878528, 'eKomi annonce un partenariat avec Google et introduit en France les évaluations des Product Listing Ads http://t.co/moGwMRdsgH', 2, 1, 0, 1, 263800084),
(582856191866482688, '"eKomi Announces #Google Partnership Introducing Product Ratings on Product Listing Ads in the UK" : http://t.co/Ldyq6Z7MoX', 2, 2, 0, 1, 40173650);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(150) NOT NULL,
  `location` varchar(150) DEFAULT NULL,
  `followers_count` int(10) unsigned NOT NULL,
  `friends_count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `location`, `followers_count`, `friends_count`) VALUES
(1059801, 'Search Engine Land', 'Everywhere search is.', 308321, 508),
(19068239, 'eKomi', 'Berlin, Germany', 1937, 1971),
(40173650, 'World News Report', '@ fb.me/specialnewsfeatures', 20145, 19448),
(44894572, 'presseportal.de', 'Hamburg', 3860, 306),
(231846311, 'Valentino''s Displays', 'Southampton, UK', 495, 1997),
(263800084, 'SXSW TOP NEWS', 'Austin, TX', 1517, 27),
(270453413, 'Allianz France', 'Paris', 30574, 315),
(461401609, 'onlynaturals.co.uk', 'Reading, UK', 835, 570),
(1407572472, 'Ecommercenews', 'Europe', 822, 119),
(2292805620, 'ERGO Deutschland', '', 3587, 232),
(2316895484, 'Allianz Avec Vous', 'France', 495, 635);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `vw_tweets_with_users`
--
CREATE TABLE IF NOT EXISTS `vw_tweets_with_users` (
`id` bigint(20) unsigned
,`text` varchar(160)
,`retweet_count` int(11)
,`favorite_count` int(11)
,`favorited` tinyint(1)
,`retweeted` tinyint(1)
,`user_id` bigint(20) unsigned
,`user_name` varchar(150)
);
-- --------------------------------------------------------

--
-- Структура для представления `vw_tweets_with_users`
--
DROP TABLE IF EXISTS `vw_tweets_with_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_tweets_with_users` AS select `tweets`.`id` AS `id`,`tweets`.`text` AS `text`,`tweets`.`retweet_count` AS `retweet_count`,`tweets`.`favorite_count` AS `favorite_count`,`tweets`.`favorited` AS `favorited`,`tweets`.`retweeted` AS `retweeted`,`tweets`.`user` AS `user_id`,`users`.`name` AS `user_name` from (`tweets` left join `users` on((`tweets`.`user` = `users`.`id`)));

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tweets`
--
ALTER TABLE `tweets`
  ADD CONSTRAINT `tweets_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
