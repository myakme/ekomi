<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 0:31
 */

$root = dirname(__FILE__);

include_once( $root .'/autoload.php' );
$config = parse_ini_file( $root . '/config.ini', true );

$database = PDOWrapper::setConnection( $config['mysql'] );
$storage = new \Model\Storage( $database );

$cache = new \Model\TwitterCache();
$cache->setOAuthOptions( $config['twitterAPI'] )
    ->setStorage( $storage )
    ->storeData( 'ekomi', 15 );

