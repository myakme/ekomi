<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 30.04.15
 * Time: 15:50
 */

/**
 * Simpliest autoloader
 */
spl_autoload_register( function($className) {

    $tokens = explode( '\\', $className );
    $path = dirname(__FILE__).'/classes';

    /**
     * check all parent directories for including some namespaces defines
     */
    foreach( $tokens as $token ) {
        $path .= '/' . $token;
        $file = $path . '.php';
        if( file_exists( $file ) ) {
            include_once($file);
        }
    }
});

