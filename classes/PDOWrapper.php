<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 0:12
 */

/**
 * Class PDOWrapper
 * Simple wrapper for database queries. It provides minimum of necessary functions for project
 */
class PDOWrapper
{
    /**
     * @var PDO
     */
    protected $pdoObject;

    /**
     * @param $options
     * @throws Exception
     * @return PDOWrapper
     */
    public static function setConnection( $options )
    {
        if( !isset($options['host']) ||
            !isset($options['database']) ||
            !isset($options['user']) ||
            !isset($options['password']) )
        {
            throw new Exception('Please check connection parameters');
        }

        $dsn = 'mysql:host=' . $options['host'] . ';dbname=' . $options['database'];
        $pdo = new PDO($dsn, $options['user'], $options['password']);
        return new PDOWrapper( $pdo );
    }

    /**
     * @param PDO $pdo
     */
    protected function __construct( \PDO $pdo )
    {
        $this->pdoObject = $pdo;
    }

    /**
     * @param $tableName
     * @param $fields
     * @param $data
     */
    public function updateMultiWithInsert( $tableName, $fields, $data )
    {
        if( count($data) > 0 ) {

            $query = 'INSERT INTO '. $tableName .' (' . implode(',' , $fields) . ') VALUES ';

            $qPart = array_fill(0, count($data), "(". trim(str_repeat('?,', count($fields)),',') .")");
            $query .=  implode(",",$qPart);

            $insertion = array();

            foreach( $data as $row ) {
                $insertion = array_merge( $insertion, array_values($row) );
            }

            $query .= ' ON DUPLICATE KEY UPDATE ';

            $updFields = array();
            foreach( $fields as $field ) {
                $updFields[] = $field . '=VALUES('.$field.')';
            }

            $query .= implode( ',', $updFields );

            $queryObj = $this->pdoObject->prepare($query);
            $queryObj->execute( $insertion );

        }
    }

    /**
     * @param $tableName
     * @param bool $order
     * @return array
     */
    public function getAll( $tableName, $order = false )
    {
        $query = 'SELECT * FROM ' . $tableName;

        if( $order ) {
            $query .= ' ORDER BY ' . $order;
        }

        $select = $this->pdoObject->query( $query );
        return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $tableName
     * @param $key
     * @return mixed
     */
    public function getOne( $tableName, $key )
    {
        $query = 'SELECT * FROM ' . $tableName . ' WHERE id='.$key;
        $select = $this->pdoObject->query( $query );
        $result = $select->fetchAll(PDO::FETCH_ASSOC);
        return $result[0];
    }

    /**
     * @param $tableName
     * @param $key
     * @param $column
     * @param bool $columnToSwitch
     */
    public function addValueById( $tableName, $key, $column, $columnToSwitch = false ) {
        $this->shiftValue($tableName, $key, $column, $columnToSwitch, false);
    }

    /**
     * @param $tableName
     * @param $key
     * @param $column
     * @param bool $columnToSwitch
     */
    public function subValueById( $tableName, $key, $column, $columnToSwitch = false ) {
        $this->shiftValue($tableName, $key, $column, $columnToSwitch, true);
    }

    /**
     * This function can increase or decrease field value, also it can set up true/false field
     *
     * @param $tableName
     * @param $key
     * @param $column
     * @param bool $columnToSwitch
     * @param bool $negative
     */
    protected function shiftValue( $tableName, $key, $column, $columnToSwitch = false, $negative = false ) {
        $character = $negative ? '-' : '+';

        $query = "UPDATE {$tableName} SET {$column}={$column}{$character}1";
        if( $columnToSwitch ) {
            $query .= ', ' . $columnToSwitch . ($negative?'=FALSE':'=TRUE');
        }
        $query .= ' WHERE id=' . $key;
        $queryObj = $this->pdoObject->prepare($query);
        $queryObj->execute();
    }
}
