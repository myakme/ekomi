<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 5:22
 */

?><!doctype html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/main.js"></script>
</head>
<body>
    <div class="container">
        <div class="row" style="margin-bottom: 40px">
            <div class="ProfileCanopy-header u-bgUserColor" style="margin-top: 0px;">
                <div class="ProfileCanopy-headerBg" style="text-align: center">
                    <img src="https://pbs.twimg.com/profile_banners/19068239/1374067796/1500x500" alt="" style="height: 300px">
                </div>
            </div>
        </div>
        <?php foreach($data as $item) { ?>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">From <?php echo $item['user_name']; ?></div>
                    <div class="panel-body">
                        <?php echo $item['text']; ?>
                    </div>
                    <div class="panel-heading">
                        <input class="twitter-btn btn btn-<?php echo $item['retweeted']?'success':'default'; ?>" type="button" value="Retweet [<?php echo $item['retweet_count']; ?>]" data-id="<?php echo $item['id']; ?>" data-title="Retweet" data-action="retweet" data-count="<?php echo $item['retweet_count']; ?>">
                        <input class="twitter-btn btn btn-<?php echo $item['favorited']?'success':'default'; ?>" type="button" value="Favorite [<?php echo $item['favorite_count']; ?>]" data-id="<?php echo $item['id']; ?>" data-title="Favorite" data-action="<?php echo $item['favorited']?'unfavorite':'favorite'; ?>" data-count="<?php echo $item['favorite_count']; ?>" <?php echo $item['favorited']?'data-flow="neg"':''; ?>>
                        <input class="answer-btn btn btn-default" type="button" value="Reply" data-id="<?php echo $item['id']; ?>">
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</body>
</html>
