<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 4:45
 */

namespace App;

/**
 * Wrapper for request
 * It usually supposed to make a data validation here
 *
 * Class Request
 * @package App
 */
class Request
{

    /**
     * current action to run
     * @var string
     */
    public $action;

    /**
     * HTTP request data
     * @var array
     */
    public $data;

    /**
     * Put some request data in constructor
     * @param array $data
     */
    public function __construct( array $data )
    {
        if( array_key_exists( 'act', $data ) ) {
            $this->action = $data['act'];
        } else {
            $this->action = 'index';
        }
        $this->data = $data;
    }

    /**
     * Magic get
     *
     * @param $name
     * @return bool
     */
    public function __get( $name )
    {
        if( array_key_exists($name, $this->data) ) {
            return $this->data[$name];
        }
        return false;
    }
}