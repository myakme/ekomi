<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 4:19
 */

namespace Controller;

/**
 * Main and only controller
 *
 * Class Main
 * @package Controller
 */
class Main
{
    /**
     * @var \App\Request
     */
    protected $request;

    /**
     * @param $request
     */
    public function __construct( \App\Request $request )
    {
        $this->request = $request;
    }

    /**
     * main page controller
     */
    public function actionIndex()
    {
        $dir = dirname(dirname(__FILE__)) . '/View/';
        $data = \App::getStorage()->getMessagesCache();

        require( $dir . 'index.php' );
    }

    /**
     * Retweet action
     */
    public function actionRetweet() {

        /**
         * send request to twitter
         */
        $twitter = \App::getTwitter();
        $response = $twitter->doRetweet( $this->request->id );
        $responseData = json_decode( $response, true );

        /**
         * No errors found in the response
         */
        if( !array_key_exists('errors', $responseData) ) {
            /**
             * Update local cache
             */
            \App::getStorage()->updateLocalRetweet( $this->request->id );
        }

        echo $response;
    }

    /**
     * Add to favorites action
     */
    public function actionFavorite() {

        /**
         * Send request to twitter
         */
        $twitter = \App::getTwitter();
        $response = $twitter->doFavorite( $this->request->id );
        $responseData = json_decode( $response, true );

        /**
         * No errors found in the response
         */
        if( !array_key_exists('errors', $responseData) ) {
            /**
             * Update local cache
             */
            \App::getStorage()->updateLocalFavoriteAdd( $this->request->id );
        }

        echo $response;

    }

    /**
     * Remove from favorites action
     */
    public function actionUnfavorite() {

        /**
         * Send request to twitter
         */
        $twitter = \App::getTwitter();
        $response = $twitter->doUnfavorite( $this->request->id );
        $responseData = json_decode( $response, true );

        /**
         * No errors found in the response
         */
        if( !array_key_exists('errors', $responseData) ) {
            /**
             * Update local cache
             */
            \App::getStorage()->updateLocalFavoriteRemove( $this->request->id );
        }

        echo $response;

    }

    /**
     * Add status as reply
     * Just sending request to twitter
     */
    public function actionReply() {
        $twitter = \App::getTwitter();
        $response = $twitter->doReply( $this->request->id, $this->request->text );
        echo $response;
    }

}