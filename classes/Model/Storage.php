<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 0:13
 */

namespace Model;

/**
 * Describes local database logic
 *
 * Class Storage
 * @package Model
 */
class Storage implements IBase
{

    /**
     * Local tables names
     */
    const USERS_TABLE = 'users';
    const TWEETS_TABLE = 'tweets';
    const TWEETS_VIEW = 'vw_tweets_with_users';

    /**
     * @var \PDOWrapper
     */
    protected $database;

    /**
     * User storage fields
     * @var array
     */
    protected $userFields = array(
        'id',
        'name',
        'location',
        'followers_count',
        'friends_count'
    );

    /**
     * Tweets storage fields
     * @var array
     */
    protected $tweetFields = array(
        'id',
        'text',
        'retweet_count',
        'favorite_count',
        'favorited',
        'retweeted',
        'user'
    );

    /**
     * @param \PDOWrapper $database
     */
    public function __construct( \PDOWrapper $database )
    {
        $this->database = $database;
    }

    /**
     * Store users from statuses
     * @param array $data
     */
    public function cacheUsers( array $data )
    {
        $this->database->updateMultiWithInsert( self::USERS_TABLE, $this->userFields, $data );
    }

    /**
     * Store tweets
     * @param array $data
     */
    public function cacheTweets( array $data )
    {
        $this->database->updateMultiWithInsert( self::TWEETS_TABLE, $this->tweetFields, $data );
    }

    /**
     * Get statuses from local cache
     * @return array
     */
    public function getMessagesCache()
    {
        return $this->database->getAll(self::TWEETS_VIEW, 'id DESC');
    }

    /**
     * Get user data by id
     * @param $id
     * @return mixed
     */
    public function getUser( $id )
    {
        return $this->database->getOne(self::USERS_TABLE, $id);
    }

    /**
     * Update status "retweet" in local cache
     * @param $id
     */
    public function updateLocalRetweet($id) {
        $this->database->addValueById(self::TWEETS_TABLE, $id, 'retweet_count', 'retweeted' );
    }

    /**
     * Update status "add to Favorite" in local cache
     * @param $id
     */
    public function updateLocalFavoriteAdd($id) {
        $this->database->addValueById(self::TWEETS_TABLE, $id, 'favorite_count', 'favorited' );
    }

    /**
     * Update status "remove from Favorite" in local cache
     * @param $id
     */
    public function updateLocalFavoriteRemove($id) {
        $this->database->subValueById(self::TWEETS_TABLE, $id, 'favorite_count', 'favorited' );
    }

}