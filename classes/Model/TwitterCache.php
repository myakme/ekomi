<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 0:13
 */

namespace Model;

/**
 * Working with timeline caching
 *
 * Class TwitterCache
 * @package Model
 */
class TwitterCache
{

    /**
     * @var array
     */
    protected $oAuthParam = array();
    /**
     * @var \Model\Storage
     */
    protected $storage;

    /**
     * @param $options
     * @return $this
     */
    public function setOAuthOptions( $options )
    {
        $this->oAuthParam = $options;
        return $this;
    }

    /**
     * @param IBase $storage
     * @internal param \PDOWrapper $database
     * @return $this
     *
     */
    public function setStorage( \Model\IBase $storage )
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @param string $user
     * @param int $limit
     */
    public function storeData( $user = '', $limit = 10 )
    {
        $getField = '?screen_name=' . $user . '&count=' . $limit;
        $twitter = new \TwitterAPIExchange($this->oAuthParam);
        $response = $twitter->setGetfield( $getField )
            ->buildOauth($this->oAuthParam['timeline_url'], 'GET')
            ->performRequest();

        $result = json_decode( $response, true );

        $users = array();
        $tweets = array();

        foreach( $result as $tweet ) {

            $user = array_key_exists('retweeted_status', $tweet) ? $tweet['retweeted_status']['user'] : $tweet['user'];

            $users[$user['id_str']] = array(
                'id' => $user['id_str'],
                'name' => $user['name'],
                'location' => $user['location'],
                'followers_count' => $user['followers_count'],
                'friends_count' => $user['friends_count']
            );

            $tweets[$tweet['id_str']] = array(
                'id' => $tweet['id_str'],
                'text' => array_key_exists('retweeted_status', $tweet) ? $tweet['retweeted_status']['text']:$tweet['text'],
                'retweet_count' => array_key_exists('retweeted_status', $tweet)?$tweet['retweeted_status']['retweet_count']:0,
                'favorite_count' => array_key_exists('retweeted_status', $tweet)?$tweet['retweeted_status']['favorite_count']:0,
                'favorited' => $tweet['favorited'],
                'retweeted' => $tweet['retweeted'],
                'user' => $user['id_str']
            );
        }

        $this->storage->cacheUsers( $users );
        $this->storage->cacheTweets( $tweets );
    }
}