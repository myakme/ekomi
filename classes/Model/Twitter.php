<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 0:13
 */

namespace Model;

/**
 * Twitter service conductor
 *
 * Class Twitter
 * @package Model
 */
class Twitter implements ITwitter
{

    /**
     * main url
     */
    const API_URL = 'https://api.twitter.com/1.1/';

    /**
     * @var array
     */
    protected $options;

    /**
     * @param $options
     */
    public function __construct( $options )
    {
        $this->options = $options;
    }

    /**
     * @param $tweet
     * @return string
     */
    public function doRetweet( $tweet )
    {
        $url = self::API_URL.'statuses/retweet/'.$tweet.'.json';

        $twitter = new \TwitterAPIExchange($this->options);
        $response = $twitter->setPostfields(array())
            ->buildOauth($url, 'POST')
            ->performRequest();

        return $response;
    }

    /**
     * @param $tweet
     * @return string
     */
    public function doFavorite( $tweet )
    {
        $url = self::API_URL.'favorites/create.json';

        $twitter = new \TwitterAPIExchange($this->options);
        $response = $twitter->setPostfields(array('id'=>$tweet))
            ->buildOauth($url, 'POST')
            ->performRequest();


        return $response;
    }

    /**
     * @param $tweet
     * @return string
     */
    public function doUnfavorite( $tweet )
    {
        $url = self::API_URL.'favorites/destroy.json';

        $twitter = new \TwitterAPIExchange($this->options);
        $response = $twitter->setPostfields(array('id'=>$tweet))
            ->buildOauth($url, 'POST')
            ->performRequest();


        return $response;
    }

    /**
     * @param $tweet
     * @param $text
     * @return string
     */
    public function doReply( $tweet, $text )
    {
        $url = self::API_URL.'statuses/update.json';

        $twitter = new \TwitterAPIExchange($this->options);
        $response = $twitter->setPostfields(array('in_reply_to_status_id'=>$tweet, 'status'=>$text))
            ->buildOauth($url, 'POST')
            ->performRequest();


        return $response;
    }


}