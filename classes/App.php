<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 4:20
 */

/**
 * Application frame
 * Class App
 */
class App
{
    /**
     * @var \Model\Storage
     */
    static protected $storage = null;

    /**
     * @var \Model\Twitter
     */
    static protected $twitter = null;

    /**
     * @param \Model\IBase $storage
     * @param \Model\ITwitter $twitter
     * @param array $options
     */
    static public function initialize( \Model\IBase $storage, \Model\ITwitter $twitter, array $options )
    {
        self::$storage = $storage;
        self::$twitter = $twitter;
    }

    /**
     * @return \Model\Storage
     * @throws Exception
     */
    static public function getStorage()
    {
        if( is_null(self::$storage) ) {
            throw new Exception('The storage is not specified!');
        }

        return self::$storage;
    }

    /**
     * @return \Model\Twitter
     * @throws Exception
     */
    static public function getTwitter()
    {
        if( is_null(self::$twitter) ) {
            throw new Exception('The storage is not specified!');
        }

        return self::$twitter;
    }

    /**
     * Simple request dispatcher
     */
    static public function run()
    {

        if( strtolower($_SERVER['REQUEST_METHOD']) == 'post' ) {
            $requestData = $_POST;
        } else {
            $requestData = $_GET;
        }

        $request = new \App\Request( $requestData );

        $controller = new \Controller\Main( $request );
        $actionName = 'action' . ucfirst($request->action);
        if( method_exists( $controller, $actionName ) ) {
            call_user_func( array( $controller, $actionName ) );
        } else {
            header('HTTP/1.0 404 Not Found');
            exit();
        }
    }
}