<?php
/**
 * Created by PhpStorm.
 * User: Akme
 * Date: 01.05.15
 * Time: 2:02
 *
 * This file describes namespace Model. Interfaces under supposed to support lower dependency level
 */

namespace Model;

/**
 * Interface IBase
 * @package Model
 */
interface IBase {
    public function cacheUsers( array $data );
    public function cacheTweets( array $data );
    public function getMessagesCache();
    public function getUser( $id );
    public function updateLocalRetweet($id);
    public function updateLocalFavoriteAdd($id);
    public function updateLocalFavoriteRemove($id);
}

/**
 * Interface ITwitter
 * @package Model
 */
interface ITwitter {
    public function doRetweet( $tweet );
    public function doFavorite( $tweet );
    public function doUnfavorite( $tweet );
    public function doReply( $tweet, $text );
}